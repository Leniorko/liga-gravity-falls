import { configureStore } from '@reduxjs/toolkit';
import { Dispatch } from 'react';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { AnyAction } from 'redux';
import { ISagasAction } from '../sagas/characters/sagasCharacters.actions';
import { rootSaga, sagaMiddleware } from '../sagas/rootSaga';
import { charactersSlice } from './characters/characters.slice';
import { filterSlice } from './controls/filter.slice';

export const store = configureStore({
  reducer: {
    characters: charactersSlice.reducer,
    filters: filterSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      thunk: false,
    }).concat(sagaMiddleware),
});

sagaMiddleware.run(rootSaga);

export function dispatchSagasAction<T>(action: ISagasAction<T>) {
  store.dispatch({ type: action.type, payload: action.payload });
}

export type TRootStore = typeof store;
export type TAppState = ReturnType<TRootStore['getState']>;
export type TAppDispatch = TRootStore['dispatch'];

export const useAppDispatch = (): Dispatch<AnyAction> => useDispatch<TAppDispatch>();

export const useAppSelector: TypedUseSelectorHook<TAppState> = useSelector;

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ICharacterFull, ICharacterSmall } from '../../interfaces/database';

export interface ICharactersInitialState {
  characters: ICharacterSmall[];
  totalPages: number;
  currentPage: number;
  itemsPerPage: number;
  isModalOpen: boolean;
  modalType?: 'new' | 'view';
  openedCharacter?: ICharacterFull;
}

const initialState: ICharactersInitialState = {
  characters: [],
  totalPages: 0,
  currentPage: 0,
  itemsPerPage: 3,
  isModalOpen: false,
  modalType: undefined,
};

// Slice related to characters page and pagination
export const charactersSlice = createSlice({
  name: 'characters',
  initialState: initialState,
  reducers: {
    setTotalPages(state, action: PayloadAction<number>) {
      state.totalPages = action.payload;
    },
    setCurrentPage(state, action: PayloadAction<number>) {
      state.currentPage = action.payload;
    },
    setModalOpenState(state, action?: PayloadAction<boolean>) {
      state.isModalOpen = action?.payload ?? !state.isModalOpen;
    },
    setModalType(state, action?: PayloadAction<'new' | 'view' | undefined>) {
      state.modalType = action?.payload;
    },
    setOpenedCharacter(state, action: PayloadAction<ICharacterFull | undefined>) {
      state.openedCharacter = action.payload;
    },
    pushNewCharacter(state, action: PayloadAction<ICharacterSmall>) {
      state.characters.push(action.payload);
    },
    setCharacters(state, action: PayloadAction<ICharacterSmall[]>) {
      state.characters = action.payload;
    },
  },
});

export const charactersActions = charactersSlice.actions;

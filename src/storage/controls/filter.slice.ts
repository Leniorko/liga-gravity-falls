import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ICharacterParameter } from '../../interfaces/database';

export interface IFilters {
  search: string[];
  gender: string[];
  race: string[];
  side: string[];
}

interface IFilterRenderData {
  genders: ICharacterParameter[];
  sides: ICharacterParameter[];
  races: ICharacterParameter[];
  search: string;
}

export interface IfilterSliceInitialState {
  toRender: IFilterRenderData;
  toSend: IFilters;
}

const initialState: IfilterSliceInitialState = {
  toRender: {
    genders: [{ id: '', value: '' }],
    sides: [{ id: '', value: '' }],
    races: [{ id: '', value: '' }],
    search: '',
  },

  toSend: {
    gender: [],
    race: [],
    search: [],
    side: [],
  },
};

// Storage for all filters
export const filterSlice = createSlice({
  name: 'filter',
  initialState,
  reducers: {
    updateRenderData(state, action: PayloadAction<IFilterRenderData | Partial<IFilterRenderData>>) {
      const newRenderData = Object.assign(state.toRender, action.payload);
      state.toRender = newRenderData;
    },
    updateFilterData(state, action: PayloadAction<IFilters | Partial<IFilters>>) {
      const newToSend = Object.assign(state.toSend, action.payload);
      state.toSend = newToSend;
    },
  },
});

export const filterActions = filterSlice.actions;

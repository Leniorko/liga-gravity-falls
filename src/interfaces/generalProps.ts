/**
 * In BEM components can have another component as a child.
 * You MUST extend props of child component with this interface in order
 * to receive parent class name
 */
export interface IChildComponentProps {
  className: string;
}

export interface ICharacterParameter {
  id: string;
  value: string;
}

export interface ICharacterFull {
  id: string;
  name: string;
  imageURL: string;
  nameColor: string;
  backgroundColor: string;
  parametersColor: string;
  description: string | null;
  gender: ICharacterParameter;
  race: ICharacterParameter;
  side: ICharacterParameter;
  tag1: string | null;
  tag2: string | null;
  tag3: string | null;
}

export interface ICharacterSmall {
  id: string;
  name: string;
  imageURL: string;
  nameColor: string;
  parametersColor: string;
  backgroundColor: string;
  gender: string;
  race: string;
  side: string;
}

import { useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navbar from './components/navbar/Navbar';
import Page404 from './pages/404/Page404';
import CharactersPage from './pages/CharactersPage';
import IndexPage from './pages/IndexPage';
import configEverything from './utils/config';

export default function App() {
  // Effect to config libraries on App load
  useEffect(() => {
    configEverything();
  }, []);

  return (
    <Router basename="/liga-gravity-falls">
      <Navbar />
      <Switch>
        <Route exact path="/">
          <IndexPage />
        </Route>
        <Route path="/characters">
          <CharactersPage />
        </Route>
        <Route path="*">
          <Page404 />
        </Route>
      </Switch>
    </Router>
  );
}

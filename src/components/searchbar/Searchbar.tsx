import { useEffect } from 'react';
import { IChildComponentProps } from '../../interfaces/generalProps';
import { filterActions } from '../../storage/controls/filter.slice';
import { useAppDispatch } from '../../storage/store';
import './searchbar.css';

interface ISearchbarProps extends IChildComponentProps {
  value: string;
  placeholder?: string;
  changeValue: (newValue: string) => void;
}

export default function Searchbar(props: ISearchbarProps) {
  const dispatch = useAppDispatch();
  useEffect(()=>{
    // NOTE: Allows to match multiword arguments wrapped with ""
    const searchQueryRexExp = new RegExp('(".+")|(\\S+)', 'gmi')
    const searchMatches = [...props.value.matchAll(searchQueryRexExp)]
    const cleanedSearchMatches = searchMatches.map((item) => item[0].replaceAll('"', ''))
    dispatch(filterActions.updateFilterData({search: cleanedSearchMatches}))
  }, [props.value])

  return (
    <input
      type="text"
      value={props.value}
      placeholder={props.placeholder ?? 'Поиск'}
      onChange={(e) => {
        props.changeValue(e.target.value);
      }}
      className={`searchbar ${props.className}`}
    />
  );
}

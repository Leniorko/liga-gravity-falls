import { Link } from 'react-router-dom';
import url from '../../img/logo.png';

export default function Navbar() {
  return (
    <div className="navbar">
      <img src={url} alt="Gravity falls logo" className="logo" />
      <Link to="" className="navbar__item">
        Главная
      </Link>
      <Link to="characters" className="navbar__item">
        Персонажи
      </Link>
    </div>
  );
}

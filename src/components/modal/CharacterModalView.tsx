import { ICharacterFull } from '../../interfaces/database';
import './characterModalView.css';
import CharacterPreview from './CharacterPreview';
import ModalContainer, { IModalContainerProps } from './ModalContainer';

interface ICharacterModalViewProps extends Omit<IModalContainerProps, 'children'> {
  character: ICharacterFull;
}

export default function CharacterModalView(props: ICharacterModalViewProps) {
  return props.character ? (
    <ModalContainer contentContainerClassName={props.contentContainerClassName} windowClassName={props.windowClassName}>
      <CharacterPreview character={props.character} />
    </ModalContainer>
  ) : (
    <></>
  );
}

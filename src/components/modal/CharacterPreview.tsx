import { ICharacterFull } from '../../interfaces/database';
import CharacterFeatures from '../paginator/itemsList/CharacterFeatures';

interface ICharacterPreviewProps {
  previewClass?: string;
  character: ICharacterFull;
}

export default function CharacterPreview(props: ICharacterPreviewProps) {
  return (
    <div className={`character-modal ${props?.previewClass}`}>
      <div className="character-modal__hero">
        <img
          src={props.character.imageURL}
          alt={`Изображение персонажа ${props.character.name}`}
          className="character-modal__img"
        />
        <h3
          className="character-modal__name"
          style={{
            color: props.character.nameColor,
            WebkitTextStroke: '2px black',
            fontSize: '48px',
            margin: '0',
            maxWidth: '15ch',
          }}
        >
          {props.character.name}
        </h3>
      </div>
      <div
        className="character-modal__info"
        style={{
          backgroundColor: props.character.backgroundColor,
          color: props.character.parametersColor,
        }}
      >
        <CharacterFeatures
          className=""
          styles={{
            backgroundColor: props.character.backgroundColor,
            color: props.character.parametersColor,
          }}
          items={[
            { fieldName: 'Пол', fieldValue: props.character.gender.value },
            { fieldName: 'Раса', fieldValue: props.character.race.value },
            { fieldName: 'Сторона', fieldValue: props.character.side.value },
          ]}
        />
        <p className="character-modal__description">{props.character.description}</p>
        <div className="character-modal__tags"></div>
      </div>
    </div>
  );
}

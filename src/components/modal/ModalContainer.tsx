import { ReactNode } from 'react';
import { useHistory } from 'react-router-dom';
import url from '../../img/x_circle.svg';
import { charactersActions } from '../../storage/characters/characters.slice';
import { useAppDispatch, useAppSelector } from '../../storage/store';
import { toMainPage } from '../../utils/navigation';
import './modalContainer.css';

export interface IModalContainerProps {
  windowClassName?: string;
  contentContainerClassName?: string;
  children: ReactNode;
}

export default function ModalContainer(props: IModalContainerProps) {
  const history = useHistory();
  const dispatch = useAppDispatch();
  const filtersState = useAppSelector((state) => state.filters);

  return (
    <div
      className="modal-window-wrapper"
      onClick={(e) => {
        // If not cleared old opened character will appear for a half a second before new one fetched
        dispatch(charactersActions.setOpenedCharacter(undefined));
        toMainPage(history, filtersState.toSend);
      }}
    >
      <div
        className={`modal-window ${props.windowClassName}`}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <img
          src={url}
          className="modal-window__close-img"
          onClick={(e) => {
            dispatch(charactersActions.setOpenedCharacter(undefined));
            toMainPage(history, filtersState.toSend);
          }}
        />

        <div className={`modal-window__content ${props.contentContainerClassName}`}>{props.children}</div>
      </div>
    </div>
  );
}

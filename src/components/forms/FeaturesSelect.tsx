import React, { ChangeEvent } from 'react';
import { ICharacterParameter } from '../../interfaces/database';

interface IFeatureSelectProps {
  className: string;
  selectName: string;
  onChange: (e: ChangeEvent) => void;
  items: ICharacterParameter[];
}

export default function FeaturesSelect(props: IFeatureSelectProps) {
  const options = props.items.map((option, index) => {
    return <option value={option.id} label={option.value} key={`option_${option.id}_${option.value}`}></option>;
  });

  return (
    <select name={props.selectName} className={props.className} onChange={props.onChange} id="">
      {options}
    </select>
  );
}

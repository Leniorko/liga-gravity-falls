import { Formik } from 'formik';
import { useHistory } from 'react-router-dom';
import * as Yup from 'yup';
import { ICharacterFull } from '../../interfaces/database';
import { SagasActionsTypes } from '../../sagas/characters/sagasCharacters.actions';
import { dispatchSagasAction, useAppSelector } from '../../storage/store';
import { toMainPage } from '../../utils/navigation';
import CharacterPreview from '../modal/CharacterPreview';
import FeaturesSelect from './FeaturesSelect';
import './newCharacterForm.css';

export default function NewCharacterForm() {
  const filtersState = useAppSelector((state) => state.filters);
  let newCharacter: ICharacterFull;
  const history = useHistory();

  const newCharacterValidationSchema = Yup.object().shape({
    nameInput: Yup.string().required('Name is required'),
    description: Yup.string()
      .required('Description is required')
      .min(10, 'Minimal length of description is 10 characters'),
    imgURL: Yup.string().url('Invalid url').required('URL is required'),
    colors: Yup.object().shape({
      name: Yup.string().required(),
      background: Yup.string().required(),
      parametrs: Yup.string().required(),
    }),
  });

  return (
    <Formik
      initialValues={{
        nameInput: '',
        gender: filtersState.toRender.genders[0].id,
        race: filtersState.toRender.races[0].id,
        side: filtersState.toRender.sides[0].id,
        description: '',
        tags: new Array<string>(),
        imgURL: '',
        colors: {
          name: '#FFFFFF',
          background: '#000000',
          parametrs: '#FFFFFF',
        },
      }}
      onSubmit={() => {
        dispatchSagasAction({ type: SagasActionsTypes.ADD_CHARACTER, payload: newCharacter });
        alert('Character created successfully!');
        toMainPage(history, filtersState.toSend);
      }}
      validationSchema={newCharacterValidationSchema}
    >
      {(props) => {
        const { values, handleChange, errors, touched, isSubmitting, handleSubmit } = props;
        newCharacter = {
          id: ``,
          imageURL: values.imgURL,
          name: values.nameInput,
          description: values.description,
          backgroundColor: values.colors.background,
          nameColor: values.colors.name,
          parametersColor: values.colors.parametrs,
          gender: {
            id: values.gender,
            value: filtersState.toRender.genders.filter((item) => item.id === values.gender)[0]?.value,
          },
          race: {
            id: values.race,
            value: filtersState.toRender.races.filter((item) => item.id === values.race)[0]?.value,
          },
          side: {
            id: values.side,
            value: filtersState.toRender.sides.filter((item) => item.id === values.side)[0]?.value,
          },
          tag1: '',
          tag2: '',
          tag3: '',
        };
        return (
          <div className="new-character-form-container">
            <form className="new-character-form" onSubmit={handleSubmit}>
              <label className="new-character-form__label-with-input">
                Добавить имя
                <input
                  type="text"
                  name="nameInput"
                  value={values.nameInput}
                  onChange={handleChange}
                  className="new-character-form__text-input"
                />
                {touched.nameInput && errors.nameInput && (
                  <div className="new-character-form__error">{errors.nameInput}</div>
                )}
              </label>
              <div className="new-character-form__info">
                <label className="new-character-form__label-with-input">
                  Пол
                  <FeaturesSelect
                    className=""
                    items={filtersState.toRender.genders}
                    selectName="gender"
                    onChange={handleChange}
                  />
                </label>
                <label className="new-character-form__label-with-input">
                  Раса
                  <FeaturesSelect
                    className=""
                    items={filtersState.toRender.races}
                    selectName="race"
                    onChange={handleChange}
                  />
                </label>
                <label className="new-character-form__label-with-input">
                  Сторона
                  <FeaturesSelect
                    className=""
                    items={filtersState.toRender.sides}
                    selectName="side"
                    onChange={handleChange}
                  />
                </label>
              </div>
              <label className="new-character-form__label-with-input">
                Описание
                <textarea
                  name="description"
                  className="new-character-form__description-input"
                  value={values.description}
                  onChange={handleChange}
                ></textarea>
                {touched.description && errors.description && (
                  <div className="new-character-form__error">{errors.description}</div>
                )}
              </label>
              <div className="new-character-form__photo-color">
                <div className="new-character-form__photo-container">
                  <img className="new-character-form__photo" src={values.imgURL} alt="" />
                  <input
                    type="text"
                    placeholder="Ссылка на картинку"
                    name="imgURL"
                    value={values.imgURL}
                    onChange={handleChange}
                    className="new-character-form__text-input"
                  />
                  {touched.imgURL && errors.imgURL && <div className="new-character-form__error">{errors.imgURL}</div>}
                </div>
                <div className="new-character-form__colors">
                  <label className="new-character-form__label-with-input">
                    Цвет имени
                    <input name="colors.name" type="color" value={values.colors.name} onChange={handleChange} />
                  </label>
                  <label className="new-character-form__label-with-input">
                    Цвет фона параметров
                    <input
                      name="colors.background"
                      type="color"
                      value={values.colors.background}
                      onChange={handleChange}
                    />
                  </label>
                  <label className="new-character-form__label-with-input">
                    Цвет параметров
                    <input
                      name="colors.parametrs"
                      type="color"
                      value={values.colors.parametrs}
                      onChange={handleChange}
                    />
                  </label>
                </div>
              </div>
              <button type="submit" className='new-character-form__submit'>Submit</button>
            </form>
            <CharacterPreview previewClass="new-character-form__preview" character={newCharacter} />
          </div>
        );
      }}
    </Formik>
  );
}

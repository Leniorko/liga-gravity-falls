import { useEffect, useState } from 'react';
import { ICharacterParameter } from '../../interfaces/database';
import './selectWithCheckboxes.css';

type TModDescription = {
  value: string[];
  changeValue: (items: string[]) => void;
};

type TSelectItem = {
  text: string;
  value: ICharacterParameter;
};

interface IGeneralSelectProps {
  reduxMode?: TModDescription;
  parentStateMode?: TModDescription;
  placeholderText: string;
  items: TSelectItem[];
}

/**
 * Select component with checkboxes.
 */
export default function GeneralSelectWithCheckboxes(props: IGeneralSelectProps) {
  const [isSelectOpen, setOpenState] = useState(false);
  const [textToShow, setTextToShow] = useState('');

  useEffect(() => {
    if (props.reduxMode?.value) {
      const currentLengthOfSelections = props.reduxMode.value?.length;
      if (currentLengthOfSelections) {
        setTextToShow(`${props.placeholderText}: ${currentLengthOfSelections}`);
      } else {
        setTextToShow(props.placeholderText);
      }
    } else {
      const currentLengthOfSelections = props.parentStateMode?.value.length;
      if (currentLengthOfSelections) {
        setTextToShow(`${props.placeholderText}: ${currentLengthOfSelections}`);
      } else {
        setTextToShow(props.placeholderText);
      }
    }
  }, [props.reduxMode?.value, props.parentStateMode?.value]);

  const selectItems = props.items.map((item) => {
    return (
      <div className="select__item" key={item.text + item.value.value}>
        <label className="select__label">
          {props.reduxMode ? (
            <input
              type="checkbox"
              id={item.value.id}
              value={item.value.id}
              defaultChecked={props.reduxMode?.value.includes(item.value.id)}
              onClick={(e) => {
                toggleSelection(props.reduxMode!, item);
              }}
            />
          ) : (
            <input
              type="checkbox"
              id={item.value.id}
              value={item.value.id}
              defaultChecked={props.parentStateMode?.value?.includes(item.value.id)}
              onClick={(e) => {
                toggleSelection(props.parentStateMode!, item);
              }}
            />
          )}
          {item.text}
        </label>
      </div>
    );
  });

  return (
    <div className="select">
      <div
        className="select__box"
        onClick={() => {
          setOpenState(!isSelectOpen);
        }}
      >
        <select className="select__select">
          <option value="">{textToShow}</option>
        </select>
        <div className="overSelect"></div>
      </div>

      {isSelectOpen ? <div className="select__checkboxes">{selectItems}</div> : <></>}
    </div>
  );
}

function toggleSelection(modToUse: TModDescription, item: TSelectItem) {
  const currentSelected = [...(modToUse.value ?? [])];
  if (!currentSelected.includes(item.value.id)) {
    currentSelected.push(item.value.id);
  } else {
    const indexOfValue = currentSelected.indexOf(item.value.id);
    currentSelected.splice(indexOfValue, 1);
  }
  modToUse.changeValue(currentSelected);
}

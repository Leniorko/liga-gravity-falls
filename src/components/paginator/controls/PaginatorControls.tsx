import left_arrow from '../../../img/left_arrow.svg';
import right_arrow from '../../../img/right_arrow.svg';
import PagesList from './PagesList';
import './paginatorControls.css';

interface IPaginatorControlsProps {
  totalPages: number;
  currentPage: number;
  setNewPage: (pageNumber: number) => void;
}

export default function PaginatorControls(props: IPaginatorControlsProps) {
  return (
    <div className="controls">
      <img
        className="controlls__arrow-left"
        onClick={() => {
          let newPage = props.currentPage - 1;
          if (newPage < 0) {
            newPage = 0;
          }
          props.setNewPage(newPage);
        }}
        src={left_arrow}
      />
      <PagesList currentPage={props.currentPage} setNewPage={props.setNewPage} totalPages={props.totalPages} />
      <img
        className="controlls__arrow-right"
        onClick={() => {
          let newPage = props.currentPage + 1;
          if (newPage < props.totalPages) {
            props.setNewPage(newPage);
          }
        }}
        src={right_arrow}
      />
    </div>
  );
}

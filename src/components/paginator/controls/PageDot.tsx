interface IPageDotProps {
  isCurrent: boolean;
  pageToOpen: number;
  setNewPage: (page: number) => void;
}

export default function PageDot(props: IPageDotProps) {
  return (
    <div
      style={{
        width: '15px',
        height: '15px',
        backgroundColor: props.isCurrent ? '#E8BD55' : 'white',
        borderRadius: '100%',
        cursor: 'pointer',
      }}
      className="page-dot"
      onClick={() => {
        props.setNewPage(props.pageToOpen);
      }}
    ></div>
  );
}

import PageDot from './PageDot';

interface IPageListProps {
  setNewPage: (page: number) => void;
  totalPages: number;
  currentPage: number;
}

export default function PagesList(props: IPageListProps) {
  if (props.totalPages === 1) {
    return <PageDot isCurrent={props.currentPage === 0} pageToOpen={0} setNewPage={props.setNewPage} />;
  } else if (props.totalPages === 2) {
    return (
      <>
        <PageDot isCurrent={props.currentPage === 0} pageToOpen={0} setNewPage={props.setNewPage} />
        <PageDot isCurrent={props.currentPage === 1} pageToOpen={1} setNewPage={props.setNewPage} />
      </>
    );
  } else if (props.totalPages >= 3) {
    return (
      <>
        <PageDot
          isCurrent={props.currentPage === 0}
          pageToOpen={calculatePageToOpen(props.currentPage, props.totalPages, 1)}
          setNewPage={props.setNewPage}
        />
        <PageDot
          isCurrent={props.currentPage > 0 && props.currentPage < props.totalPages - 1}
          pageToOpen={calculatePageToOpen(props.currentPage, props.totalPages, 2)}
          setNewPage={props.setNewPage}
        />
        <PageDot
          isCurrent={props.currentPage === props.totalPages - 1}
          pageToOpen={calculatePageToOpen(props.currentPage, props.totalPages, 3)}
          setNewPage={props.setNewPage}
        />
      </>
    );
  }

  return <></>;
}

function calculatePageToOpen(currentPage: number, totalPages: number, position: number): number {
  if (position === 1) {
    if (currentPage === totalPages - 1) {
      return currentPage - 2;
    } else if (currentPage - 1 !== -1) {
      return currentPage - 1;
    }
  }
  if (position === 2) {
    if (currentPage === 0) {
      return currentPage + 1;
    } else if (currentPage === totalPages - 1) {
      return currentPage - 1;
    }
  }
  if (position === 3) {
    if (currentPage === 0) {
      return currentPage + 2;
    } else if (currentPage + 1 <= totalPages - 1) {
      return currentPage + 1;
    }
  }
  return currentPage;
}

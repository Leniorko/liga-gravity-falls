import { ICharacterSmall } from '../../interfaces/database';
import { charactersActions } from '../../storage/characters/characters.slice';
import { useAppDispatch, useAppSelector } from '../../storage/store';
import PaginatorControls from './controls/PaginatorControls';
import CardList from './itemsList/CardList';
import './paginator.css';

interface IPaginatorProps {
  characterList: ICharacterSmall[];
}

export default function Paginator(props: IPaginatorProps) {
  const paginatorState = useAppSelector((state) => state.characters);
  const dispatch = useAppDispatch();

  return (
    <div className="paginator">
      <CardList items={props.characterList} />
      <PaginatorControls
        currentPage={paginatorState.currentPage}
        totalPages={paginatorState.totalPages}
        setNewPage={(newPage: number) => dispatch(charactersActions.setCurrentPage(newPage))}
      />
    </div>
  );
}

import { useHistory } from 'react-router-dom';
import { ICharacterSmall } from '../../../interfaces/database';
import { SagasActionsTypes } from '../../../sagas/characters/sagasCharacters.actions';
import { dispatchSagasAction, useAppDispatch } from '../../../storage/store';
import './characterCard.css';
import CharacterFeatures from './CharacterFeatures';

interface ICharacterCardProps {
  character: ICharacterSmall;
}

export default function CharacterCard(props: ICharacterCardProps) {
  const dispatch = useAppDispatch();
  const history = useHistory();

  return (
    <div
      className="card"
      onClick={() => {
        dispatchSagasAction({ type: SagasActionsTypes.GET_AND_SET_CHARACTER, payload: props.character.id });
        history.push(`characters/view/${props.character.id}`);
      }}
    >
      <div className="card__heading">
        <img
          src={props.character.imageURL}
          alt={`Изображение ${props.character.name}`}
          className="card__character-img"
        />
        <h3 className="card__character-name" style={{ color: props.character.nameColor }}>
          {props.character.name}
        </h3>
      </div>
      <CharacterFeatures
        className="card__info"
        styles={{
          backgroundColor: props.character.backgroundColor,
          color: props.character.parametersColor,
        }}
        items={[
          { fieldName: 'Пол', fieldValue: props.character.gender },
          { fieldName: 'Раса', fieldValue: props.character.race },
          { fieldName: 'Сторона', fieldValue: props.character.side },
        ]}
      />
    </div>
  );
}

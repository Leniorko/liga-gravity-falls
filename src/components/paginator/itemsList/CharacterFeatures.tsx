import { CSSProperties } from 'react';
import { IChildComponentProps } from '../../../interfaces/generalProps';
import './characterFeatures.css';

interface ICharacterFeaturesProps extends IChildComponentProps {
  items: {
    fieldName: string;
    fieldValue: string;
  }[];
  styles: Pick<CSSProperties, 'backgroundColor' | 'color'>;
}

export default function CharacterFeatures(props: ICharacterFeaturesProps) {
  const featuresList = props.items.map((item, index, array) => {
    return (
      <div className="feature-wrapper" key={item.fieldName + item.fieldValue + index}>
        <p className={'feature'}>
          <span className="feature__name">{item.fieldName}</span>
          <span className="feature__value">{item.fieldValue}</span>
        </p>
        {index === array.length - 1 ? (
          <></>
        ) : (
          <div style={{ backgroundColor: props.styles.color }} className="feature__delimeter"></div>
        )}
      </div>
    );
  });

  return (
    <div style={props.styles} className={`fields ${props.className}`}>
      {featuresList}
    </div>
  );
}

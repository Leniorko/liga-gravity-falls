import { ICharacterSmall } from '../../../interfaces/database';
import './cardList.css';
import CharacterCard from './CharacterCard';

interface ItemListProps {
  items: ICharacterSmall[];
}

export default function CardList(props: ItemListProps) {
  const cards = props.items.map((value, index) => {
    return <CharacterCard character={value} key={`${value.name}_${index}`} />;
  });

  return <div className="card-list">{cards}</div>;
}

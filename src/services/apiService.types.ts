import { ICharacterFull, ICharacterSmall, ICharacterParameter } from '../interfaces/database';

export interface IGetCharactersRequestParams {
  values?: string[];
  gender?: string[];
  race?: string[];
  side?: string[];
  sort?: 'createdDate,ASC';
  page: number;
  size: number;
}

export interface IPostCharacterParams {
  id: string;
  name: string;
  description: string;
  imageURL: string;
  nameColor: string;
  backgroundColor: string;
  parametersColor: string;
  tag1: string;
  tag2: string;
  tag3: string;
  gender: {
    id: string;
    value: string;
  };
  race: {
    id: string;
    value: string;
  };
  side: {
    id: string;
    value: string;
  };
}

export type TGetCharacterId = string;

export type TParametrResponce = ICharacterParameter[];

export interface ICharacterListGetResponce {
  content: ICharacterSmall[];
  pageable: {
    sort: {
      sorted: boolean;
      unsorted: boolean;
      empty: boolean;
    };
    pageNumber: number;
    pageSize: number;
    offset: number;
    paged: boolean;
    unpaged: boolean;
  };
  last: boolean;
  totalPages: number;
  totalElements: number;
  first: boolean;
  sort: {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
  };
  number: number;
  numberOfElements: number;
  size: number;
  empty: boolean;
}

export type TCharacterPostResponce = string;

export type TCharacterGetResponce = ICharacterFull;

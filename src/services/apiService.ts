import axios from 'axios';
import { ICharacterSmall } from '../interfaces/database';
import {
  ICharacterListGetResponce,
  IGetCharactersRequestParams,
  IPostCharacterParams,
  TGetCharacterId,
  TParametrResponce,
} from './apiService.types';

const BASE_URL = 'https://liga-backend.herokuapp.com/api/GRAVITY_FALLS/';

const apiActions = {
  getCharactersByPage: async function (
    params: IGetCharactersRequestParams = {
      page: 0,
      size: 3,
    }
  ): Promise<ICharacterListGetResponce> {
    const url = BASE_URL + 'character';
    const data = await axios.get(url, { params }).then((response) => response.data);
    return data;
  },

  getCharacterById: async function (id: TGetCharacterId): Promise<ICharacterSmall> {
    const url = BASE_URL + 'character/' + id;
    const characterData = await axios.get(url).then((responce) => responce.data);
    return characterData;
  },

  getGenders: async function (): Promise<TParametrResponce> {
    const url = BASE_URL + 'gender';
    const gendersList = await axios.get(url).then((response) => response.data);
    return gendersList;
  },

  getRaces: async function (): Promise<TParametrResponce> {
    const url = BASE_URL + 'race';
    const racesList = await axios.get(url).then((response) => response.data);
    return racesList;
  },
  getSides: async function (): Promise<TParametrResponce> {
    const url = BASE_URL + 'side';
    const sidesList = await axios.get(url).then((response) => response.data);
    return sidesList;
  },

  postCharacter: async function (characterParams: IPostCharacterParams): Promise<string> {
    const url = BASE_URL + 'character';
    const createdCharacterId = await axios.post(url, characterParams).then((responce) => responce.data);
    return createdCharacterId;
  },
};

export default apiActions;

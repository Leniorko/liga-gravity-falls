import { useHistory } from 'react-router';

export default function IndexPage() {
  const history = useHistory();

  return (
    <div className="main">
      <h1 className="main__heading">Найди любимого персонажа "Gravity Falls"</h1>
      <p className="main__description">Вы сможете узнать тип героев, их способности, сильные стороны инедостатки.</p>
      <button
        className="main__button"
        onClick={() => {
          history.push('/characters');
        }}
      >
        Начать
      </button>
    </div>
  );
}

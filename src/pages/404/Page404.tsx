import { useHistory } from 'react-router';
import url from '../../img/404.png';

export default function Page404() {
  const history = useHistory();

  return (
    <div className="not-found">
      <img src={url} alt="" className="not-found__img" />
      <p className="not-found__message">Ошибка 404. Такая страница не существует либо она была удалена</p>
      <button className="not-found__button" onClick={() => history.push('/')}>
        На главную
      </button>
    </div>
  );
}

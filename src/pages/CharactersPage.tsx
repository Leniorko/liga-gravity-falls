import qs from 'qs';
import { useEffect, useState } from 'react';
import { Route, Switch, useHistory, useRouteMatch } from 'react-router-dom';
import NewCharacterForm from '../components/forms/NewCharacterForm';
import CharacterModalView from '../components/modal/CharacterModalView';
import ModalContainer from '../components/modal/ModalContainer';
import Paginator from '../components/paginator/Paginator';
import Searchbar from '../components/searchbar/Searchbar';
import GeneralSelectWithCheckboxes from '../components/select/SelectWithCheckboxes';
import { SagasActionsTypes } from '../sagas/characters/sagasCharacters.actions';
import { charactersActions } from '../storage/characters/characters.slice';
import { filterActions, IfilterSliceInitialState } from '../storage/controls/filter.slice';
import { dispatchSagasAction, useAppDispatch, useAppSelector } from '../storage/store';
import { getPage, openModalWithNewForm, toMainPage } from '../utils/navigation';
import './charactersPage.css';

export default function CharactersPage() {
  const charactersState = useAppSelector((state) => state.characters);
  const filtersState = useAppSelector((state) => state.filters);
  const dispatch = useAppDispatch();
  const history = useHistory();
  const { openedCharacterId } = useRouteMatch<{ openedCharacterId: string }>().params;
  const [pageFirstLoad, setFirstLoad] = useState(true);

  useEffect(() => {
    dispatchSagasAction({ type: SagasActionsTypes.GET_AND_SET_PARAMS });
  }, []);

  useEffect(() => {
    let filtersToShow = qs.stringify(filtersState.toSend, { addQueryPrefix: true });
    if (pageFirstLoad) {
      firstLoadStorageSetup(filtersState, filtersToShow, dispatch, setFirstLoad, history, openedCharacterId);
    } else {
      if (history.location.search !== filtersToShow) {
        toMainPage(history, filtersState.toSend);
      }
    }

    // This will create one additional request.
    getPage(charactersState, filtersState);
  }, [filtersState.toSend, charactersState.currentPage]);

  // If you was in 3 page and total pages changed to 2 pages. This will force you to 2 page.
  useEffect(() => {
    if (charactersState.currentPage >= charactersState.totalPages && charactersState.currentPage - 1 >= 0) {
      dispatch(charactersActions.setCurrentPage(charactersState.totalPages - 1));
    }
  }, [charactersState.totalPages]);

  return (
    <>
      {/* Modal window switch to not cause rerender.
      I know that is proper to get current path. But this is simplier for now. */}
      <Switch>
        <Route path="/characters/new">
          <ModalContainer>
            <NewCharacterForm />
          </ModalContainer>
        </Route>
        <Route path={'/characters/view/:openedCharacterId'}>
          <CharacterModalView character={charactersState.openedCharacter!} windowClassName="characters__preview" />
        </Route>
      </Switch>
      <div className="characters">
        <div className="characters__inputs">
          <Searchbar
            className="characters__search"
            value={filtersState.toRender.search}
            changeValue={(newValue: string) => {
              dispatch(filterActions.updateRenderData({ search: newValue }));
            }}
          />
          <GeneralSelectWithCheckboxes
            reduxMode={{
              value: filtersState.toSend.gender,
              changeValue: (items: string[]) => {
                dispatch(filterActions.updateFilterData({ gender: items }));
              },
            }}
            placeholderText="Пол"
            items={[
              { text: 'Мужской', value: filtersState.toRender.genders[0] },
              { text: 'Женский', value: filtersState.toRender.genders[1] ?? filtersState.toRender.genders[0] },
              { text: 'Неизвестно', value: filtersState.toRender.genders[2] ?? filtersState.toRender.genders[0] },
            ]}
          />
          <GeneralSelectWithCheckboxes
            reduxMode={{
              value: filtersState.toSend.race,
              changeValue: (items: string[]) => {
                dispatch(filterActions.updateFilterData({ race: items }));
              },
            }}
            placeholderText="Раса"
            items={[
              { text: 'Человек', value: filtersState.toRender.races[0] },
              { text: 'Монстр', value: filtersState.toRender.races[1] ?? filtersState.toRender.races[0] },
              { text: 'Неизвестно', value: filtersState.toRender.races[2] ?? filtersState.toRender.races[0] },
            ]}
          />
          <GeneralSelectWithCheckboxes
            reduxMode={{
              value: filtersState.toSend.side,
              changeValue: (items: string[]) => {
                dispatch(filterActions.updateFilterData({ side: items }));
              },
            }}
            placeholderText="Сторона"
            items={[
              { text: 'Порядок', value: filtersState.toRender.sides[0] },
              { text: 'Хаос', value: filtersState.toRender.sides[1] ?? filtersState.toRender.sides[0] },
              { text: 'Неизвестно', value: filtersState.toRender.sides[2] ?? filtersState.toRender.sides[0] },
            ]}
          />
          <button
            className="characters__add-btn"
            onClick={() => {
              openModalWithNewForm(history);
            }}
          >
            <span>Добавить</span>
            <span>+</span>
          </button>
        </div>
        <Paginator characterList={charactersState.characters} />
      </div>
    </>
  );
}

function firstLoadStorageSetup(
  filtersState: IfilterSliceInitialState,
  filtersToShow: string,
  dispatch: ReturnType<typeof useAppDispatch>,
  setFirstLoad: (state: boolean) => void,
  history: ReturnType<typeof useHistory>,
  openedCharacterId: string
) {
  if (history.location.search !== filtersToShow) {
    // Мы получаем данные с поисковой строки
    const params = qs.parse(history.location.search, { ignoreQueryPrefix: true });
    // И вставляем их в бд
    dispatch(filterActions.updateFilterData({ ...filtersState.toSend, ...params }));
    toMainPage(history, filtersState.toSend);
  }
  setFirstLoad(false);
}

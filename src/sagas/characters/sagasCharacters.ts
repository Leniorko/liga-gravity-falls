import { call, put, takeEvery } from "redux-saga/effects";
import { ICharacterFull } from "../../interfaces/database";
import apiActions from "../../services/apiService";
import { ICharacterListGetResponce, IGetCharactersRequestParams, IPostCharacterParams, TCharacterPostResponce, TGetCharacterId, TParametrResponce } from "../../services/apiService.types";
import { charactersActions } from "../../storage/characters/characters.slice";
import { filterActions } from "../../storage/controls/filter.slice";
import { ISagasAction, SagasActionsTypes } from "./sagasCharacters.actions";

function* getPage(action: ISagasAction<IGetCharactersRequestParams>) {
  const response: ICharacterListGetResponce = yield call(apiActions.getCharactersByPage, action.payload);
  yield put(charactersActions.setCharacters(response.content));
  yield put(charactersActions.setTotalPages(response.totalPages));
}

function* getCharacter(action: ISagasAction<TGetCharacterId>) {
  if (action.payload) {
    const response: ICharacterFull = yield call(apiActions.getCharacterById, action.payload);
    yield put(charactersActions.setOpenedCharacter(response));
  }
}

function* getParams(action: ISagasAction) {
  const genders: TParametrResponce = yield call(apiActions.getGenders);
  const races: TParametrResponce = yield call(apiActions.getRaces);
  const sides: TParametrResponce = yield call(apiActions.getSides);
  yield put(
    filterActions.updateRenderData({
      genders,
      races,
      sides,
    })
  );
}

function* addCharacter(action: ISagasAction<IPostCharacterParams>) {
  const newCharacterId: TCharacterPostResponce = yield call(apiActions.postCharacter, action.payload!);
}

// Sagas watcher that calls every worker
export default function* watchGetCharacters() {
  yield takeEvery(SagasActionsTypes.GET_AND_SET_PAGE, getPage);
  yield takeEvery(SagasActionsTypes.GET_AND_SET_CHARACTER, getCharacter);
  // yield takeEvery(sagasActionsTypes.GET_GENDERS, getGender);
  // yield takeEvery(sagasActionsTypes.GET_RACES, getRaces);
  // yield takeEvery(sagasActionsTypes.GET_SIDES, getSides);
  yield takeEvery(SagasActionsTypes.ADD_CHARACTER, addCharacter);
  yield takeEvery(SagasActionsTypes.GET_AND_SET_PARAMS, getParams);
}
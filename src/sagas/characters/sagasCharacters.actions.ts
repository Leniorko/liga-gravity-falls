export enum SagasActionsTypes {
  GET_AND_SET_PAGE = 'GET_AND_SET_PAGE', // Uses for pagination purpose
  GET_AND_SET_CHARACTER = 'GET_AND_SET_CHARACTER', // Uses when modal window needed to open
  // GET_GENDERS = 'GET_GENDERS',
  // GET_RACES = 'GET_RACES',
  // GET_SIDES = 'GET_SIDES',
  GET_AND_SET_PARAMS = 'GET_AND_SET_PARAMS',
  ADD_CHARACTER = 'ADD_CHARACTER',
  DELETE_CHARACTER = 'DELETE_CHARACTER',
}

export interface ISagasAction<T = any> {
  type: SagasActionsTypes;
  payload?: T;
}

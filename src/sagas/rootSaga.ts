import createSagaMiddleware from '@redux-saga/core';
import { all } from 'redux-saga/effects';
import watchGetCharacters from './characters/sagasCharacters';

export const sagaMiddleware = createSagaMiddleware();

export function* rootSaga() {
  yield all([watchGetCharacters()]);
}

// function* getGender(action: sagasAction) {
//   const responce: TParametrResponce = yield call(apiActions.getGenders);
//   yield put(charactersActions.setGendersToDisplay(responce));
// }

// function* getRaces(action: sagasAction) {
//   const responce: TParametrResponce = yield call(apiActions.getRaces);
//   yield put(charactersActions.setRacesToDisplay(responce));
// }

// function* getSides(action: sagasAction) {
//   const responce: TParametrResponce = yield call(apiActions.getSides);
//   yield put(charactersActions.setSidesToDisplay(responce));
// }

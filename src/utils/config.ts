import axios from 'axios';
import QueryString from 'qs';

export default function configEverything() {
  configAxios();
}

function configAxios() {
  axios.defaults.paramsSerializer = function (params) {
    return QueryString.stringify(params, { indices: false }); // param=value1&param=value2
  };
}

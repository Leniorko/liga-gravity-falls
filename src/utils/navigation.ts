/**
 * This file contains navigation helpers.
 */

import QueryString from 'qs';
import { useHistory } from 'react-router-dom';
import { SagasActionsTypes } from '../sagas/characters/sagasCharacters.actions';
import { IGetCharactersRequestParams } from '../services/apiService.types';
import { IFilters } from '../storage/controls/filter.slice';
import { dispatchSagasAction, TAppState } from '../storage/store';

type THistory = ReturnType<typeof useHistory>;

/**
 * Receives method to change current path and filters to use. Do actual navigation to the page
 * @param push
 * @param filtersToShow
 */
export function toMainPage(history: THistory, filtersData: IFilters) {
  const filtersToShow = QueryString.stringify(filtersData, { addQueryPrefix: true });
  history.push({
    pathname: '/characters',
    search: filtersToShow,
  });
}

export function openModalWithNewForm(history: THistory) {
  history.push('/characters/new');
}

/**
 * Wrapper function to actionDispatch. Fetching and setting data at characters page
 * @param charactersState
 * @param filtersState
 * @param searchArray
 */
export function getPage(
  charactersState: TAppState['characters'],
  filtersState: TAppState['filters'],
  searchArray?: string[]
) {
  dispatchSagasAction({
    type: SagasActionsTypes.GET_AND_SET_PAGE,
    payload: {
      page: charactersState.currentPage,
      size: charactersState.itemsPerPage,
      gender: filtersState.toSend.gender,
      race: filtersState.toSend.race,
      side: filtersState.toSend.side,
      values: filtersState.toSend.search,
    } as IGetCharactersRequestParams,
  });
}
